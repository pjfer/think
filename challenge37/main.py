import sys
import math


INPUT = sys.argv[1]
OUTPUT = "team12_que_nerd/challenge37/result.txt"


file = open(INPUT, "r")
data = file.readlines()
file.close()

number_balls = int(data[0].replace("\n", ""))
differences = sorted([int(n) for n in data[1].split(" ")], reverse=True)

result = [0]

index = 0
for d in differences:
   result.append(d - result[index])
   index += 1

for r in result:
   if r < 0:
      result.remove(r)

file_output = open(OUTPUT, "w")

if len(result) == len(differences):
   file_output.write(f"{str(sorted(result))[1:-1].replace(',', '')}\n")
else:
   file_output.write("Incorrect Balance\n")
