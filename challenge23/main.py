import sys

INPUT = sys.argv[1]
OUTPUT = "result.txt"            # team12_que_nerd/challenge23/

calculated_masks = {}


def find_best_shots(data, mask, current_index=0):
   my_mask = mask.copy()
   my_mask[current_index] = '1'
   if current_index + 1 != len(data):
      my_mask[current_index + 1] = '0'

   max_sum = 0
   max_mask = my_mask.copy()
   for i in range(current_index + 2, min(len(data), current_index + 4)):
      my_mask[i - 1] = '0'
      # if i in calculated_masks and calculated_masks[i][1] > max_sum:
      #    max_sum = calculated_masks[i][1]
      #    max_mask[i:] = calculated_masks[i][0]
      # else:
      to_sum, resultant_mask = find_best_shots(data, my_mask, i)
      if to_sum > max_sum:
         max_mask[i:] = [number for number in resultant_mask[i:]]
         max_mask[i - 1] = '0'
         max_sum = to_sum
         calculated_masks[i] = (resultant_mask[i:], max_sum)
   
   return max_sum + data[current_index], max_mask


file = open(INPUT, "r")
data = file.readlines()
file.close()

number_objects = int(data[0].replace("\n", ""))
objects_prices = [int(price) for price in data[1].split(" ")]


file_output = open(OUTPUT, "w")

result = [None]*2
mask = [None]*2
result[0], mask[0] = find_best_shots(objects_prices, ['x']*len(objects_prices))
result[1], mask[1] = find_best_shots(objects_prices, ['x']*len(objects_prices), 2)

save = 0
if result[1] > result[0]:
   save = 1
file_output.write(f"{result[save]}\n")

result_objects = ""
for i in range(len(mask[save])):
   if mask[save][i] == '1':
      result_objects += f"{objects_prices[i]} "
   else:
      result_objects += f"0 "

file_output.write(f"{result_objects[0:-1]}\n")

file_output.close()
