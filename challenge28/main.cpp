#include <bits/stdc++.h>
#include <math.h>
using namespace std;




int main(int argc, char** argv) {


    ifstream file;

    file.open(argv[1]);

	int limit = 0;
	file >> limit;


	int sequence_size = 0;
	int maximum_size = 0;
	int biggest_number = 0;
	cout << limit << endl;


	for (int i = 1 ; i < limit + 1 ; i++) {
		unsigned long long n = i;
		int starting_value = i;

		while(1) {
			sequence_size += 1;

			if (n & 1)
				n = 3 * n + 1;
			else
				n = n >> 1;
			
			if (n == 1) {
				if (maximum_size < sequence_size){
					maximum_size = sequence_size;
					biggest_number = starting_value;
				}
				sequence_size = 0;
				break;
			}
		}
	}


	//cout << counter2 << endl;
    ofstream outputFile("result.txt");


	outputFile << biggest_number << " "  << maximum_size << endl;

    return 0;
}