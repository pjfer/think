import sys
from collections import OrderedDict


with open(sys.argv[1], 'r') as f:
    colors = f.readline().replace('\n', ' ').split(" ")
    states = f.read().replace('\n', ' ').split(" ")
    adjacencies = OrderedDict()
    countries_colored = OrderedDict()
    free_colors = list(range(len(colors)))
    adjacent_states = list()


    for c1,c2 in zip(states[::2], states[1::2]):
        adjacent_states.append(c2)

        if c1 in adjacencies.keys():
            adjacencies[c1].append(c2)
        else:
            adjacencies[c1] = [c2]

    for state in states:
        countries_colored[state] = -1

    for state in states:
        if state in adjacencies.keys():
            for st in adjacencies[state]:
                if countries_colored[st] != -1 and countries_colored[st] in free_colors:
                    free_colors.remove(countries_colored[st])

        if state in adjacent_states:
            for st in adjacencies.keys():
                if state in adjacencies[st] and countries_colored[st] != -1 and countries_colored[st] in free_colors:
                    free_colors.remove(countries_colored[st])

        if len(free_colors) > 0:
            countries_colored[state] = free_colors[0]

        free_colors = list(range(len(colors)))

    with open('team12_que_nerd/challenge14/result.txt', 'w+') as fp:
        if -1 in countries_colored.values():
            sys.exit(0)
        else:
            for state, color_id in countries_colored.items():
                result = "{} {}\n".format(state, colors[color_id])
                print(result)
                fp.write(str(result))

        fp.write("\n")
