#include <bits/stdc++.h>
#include <math.h>
using namespace std;


                                                             
int main(int argc, char** argv) {


    ifstream file;

    file.open(argv[1]);

    int linhas , colunas ; 
    file >> linhas >> colunas;

    vector<vector<int>>  grid(linhas,vector<int>(colunas));

    for(int y = 0 ; y < linhas ; y++) {
        for(int x = 0 ; x < colunas; x++) {
            int value ;
            file >> value;
            
            grid[y][x] = value;
        }
    }


    

    vector<vector<int>>  sub_arrays;

    vector<int> arr;
    vector<int> v;

    int n = linhas;
    for(int i = 0 ; i < n ; i++)
        arr.push_back(i);

    for (int i=0; i <n; i++) 
    { 
        
        for (int j=i; j<n; j++) 
        { 
            vector<int> v;
            for (int k=i; k<=j; k++) 
                v.push_back(arr[k]);

            sub_arrays.push_back(v);
            
        } 
    } 

    

    vector<int> best;
    int best_score = -1;
    int best_start_index = -1;
    int best_end_index = -1;
    for(auto &sub_array : sub_arrays) {
        vector<int> cummulative_sum(colunas,0);
        for(int &y : sub_array ) {
            for(int x = 0 ; x < colunas ; x++) 
                cummulative_sum[x] += grid[y][x];       
            }

          int max_so_far = INT_MIN, max_ending_here = 0,  start_index =0, end_index = 0, s=0; 

          for (int i=0; i< cummulative_sum.size(); i++ ) 
          { 
              max_ending_here += cummulative_sum[i]; 

              if (max_so_far < max_ending_here) 
              { 
                  max_so_far = max_ending_here; 
                  start_index = s; 
                  end_index = i; 
              } 

              if (max_ending_here < 0) 
              { 
                  max_ending_here = 0; 
                  s = i + 1; 
              } 
          } 
        
          if(best_score < max_so_far){
              best_score= max_so_far;
              best = sub_array;
              best_start_index = start_index;
              best_end_index = end_index;
          }



      }

   int y_initial = best[0]; 
   int y_final = best[best.size() -1 ];
   int x_initial = best_start_index;
   int x_final = best_end_index;
   int total_score = 0;
   for(int &y : best) {
       for(int x = x_initial ; x <= x_final ; x++) 
           total_score+=grid[y][x];
    }
   
  // cout << total_score << endl;
  // cout << y_initial << " " << x_initial << " " <<  y_final << " " << x_final << endl;
  


    ofstream outputFile("result.txt");

    outputFile << total_score << endl;
    outputFile << y_initial << " " << x_initial << " " <<  y_final << " " << x_final << endl;
  

    return 0;
}