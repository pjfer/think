import sys

INPUT = sys.argv[1]
OUTPUT = "team12_que_nerd/challenge22/result.txt"

def get_ones_index(number):
   result = ""
   size = 0
   bin_list = list(bin(number))[2:]
   for i in range(len(bin_list)):
      if bin_list[i] == '1':
         result += f"{i} "
         size += 1
   return result[0:-1], size

def is_impossible(number):
   result = False
   treshold = 3
   current_equals = 0

   for n in list(bin(number))[2:]:
      if n == '1':
         current_equals += 1
      else:
         current_equals = 0
      if current_equals == treshold:
         return True
   return False


file = open(INPUT, "r")
data = file.readlines()
file.close()

file_output = open(OUTPUT, "w")

number = int(data[0].replace("\n", ""))

if number%4 == 3:
   file_output.write(f"-1\n")
   file_output.close()
else:
   one = 1
   while True:
      current = 2**(one)
      for s in range(one, 0, -1):
         result = current + (1 << s - 1)
         if result%number == 0:
            file_output.write(f"{s - 1} {one}\n")
            file_output.close()
            sys.exit(1)
      one += 1

file_output.close()
