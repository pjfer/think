import sys
import string


with open(sys.argv[1], 'r') as f:
    next_words = dict()
    punctuation = '''!()[]{};:'"\,<>./?@#$%^&*_~”“\n’'''
    words = f.read().split(" ")
    words = [word.translate(str.maketrans('', '', punctuation)) for word in words]

    for w1, w2 in zip(words[::2], words[1::2]):
        if len(w1) > 2 and len(w2) > 2:
            if w1 in next_words.keys():
                if w2 in next_words[w1].keys():
                    next_words[w1][w2] += 1
                else:
                    next_words[w1][w2] = 1
            else:
                next_words[w1] = dict()

    with open('team12_que_nerd/challenge21/result.txt', 'w+') as fp:
        for w1 in next_words.keys():
            for w2, count in next_words[w1].items():
                fp.write("%s={%s=%d}\n" % (w1, w2, count))
    
        fp.write("\n")
