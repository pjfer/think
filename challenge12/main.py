import sys

with open(sys.argv[1], 'r') as f:
    for line in f:
        values = line.split(" ")
        n = int(values[0])
        m = int(values[1])
        t = int(values[2])
        total_num = 0
        num = n ** m

        for base in range(3, num):
            res = int(num, base)
            print(res)

            if len(str(res)) - 1 < t:
                continue
            else:
                num_zeros = 0

                for i in range(len(str(res)) - 1):
                    if num_zeros == t + 1:
                        break
                    elif str(res)[-i] == '0':
                        num_zeros += 1

                if num_zeros == t:
                    total_num += 1
        
        print(total_num)
        
        with open('team12_que_nerd/challenge12/result.txt', 'w+') as fp:
                fp.write(str(total_num))