import sys
import random
import math


def map(n, codes, words, word_mapping):
    unused_codes = codes.copy()

    for i in range(n):
        code = unused_codes[random.randint(1, len(unused_codes) - 1)]
        unused_codes.remove(code)
        word_mapping[words[i][0]] = code

    codes = unused_codes.copy()

    for word in words:
        unused_codes = codes.copy()

        for char in word[1:]:
            code = unused_codes[random.randint(0, len(unused_codes) - 1)]
            unused_codes.remove(code)
            word_mapping[char] = code

    return word_mapping

def verify(n, words, word_mapping):
    first_sum = 0
    second_sum = 0
    overflow = 0

    for i in range(n-2):
        for j in range(1, len(words[:n-1][0]) + 1):
            value = word_mapping[words[i][-j]] + word_mapping[words[i+1][-j]]

            if overflow == 1:
                value += overflow
                overflow = 0

            if value > 9:
                overflow = 1
                value -= 10

            if value != word_mapping[words[n-1][-j]]:
                return False

    return True


words = list()
results = list()
word_mapping = dict()
codes = list(range(10))
n = 0

with open(sys.argv[1], 'r') as f:
    n = int(f.readline())

    if 3 > n > 10:
        sys.exit(1)

    for line in f:
        word_chars = list(line.replace("\n", ""))

        if len(word_chars) > 10:
            sys.exit(1)

        words.append(word_chars)

unique_chars = list(set([char for word in words for char in word]))

if len(unique_chars) > 10:
    sys.exit(1)

for i in range(math.factorial(len(unique_chars))):
    if word_mapping == dict():
        word_mapping = map(n, codes, words, word_mapping)
    else:
        new_words_mapping = map(n, codes, words, word_mapping)

        while len(set(new_words_mapping.items()) ^ set(word_mapping.items())) != 0 and new_words_mapping in results:
            new_words_mapping = map(n, codes, words, word_mapping)

        word_mapping = new_words_mapping.copy()

    if verify(n, words, word_mapping):
        results.append(word_mapping)

print(results)

'''
with open('team12_que_nerd/challenge32/result.txt', 'w+') as fp:
    fp.write("%s\n" % results)
'''
