import sys
from collections import Counter
from collections import OrderedDict
from operator import itemgetter
import decimal

def verify(quo, atom, left_equation, right_equation):
    if right_equation != '':
        for molecules in right_equation:
            if atom in molecules.keys():
                molecules[atom] *= int(quo)

left_equation = [dict()]
right_equation = [dict()]
left_side = True
molecule_id = 0
multiplier = 1

with open(sys.argv[1], 'r') as f:
    for line in f:
        atom = ""
        molecule = ""

        for char in line:
            if char.isdigit():
                if atom == "":
                    multiplier = int(char)
                
                if left_side:
                    if atom in left_equation[molecule_id]:
                        left_equation[molecule_id][atom] += multiplier * int(char)
                    else:
                        left_equation[molecule_id][atom] = multiplier * int(char)
                else:
                    if atom in right_equation[molecule_id]:
                        right_equation[molecule_id][atom] += multiplier * int(char)
                    else:
                        right_equation[molecule_id][atom] = multiplier * int(char)
                atom = ""
            elif atom != "" and (char >= "A" and char <= "Z"):
                if left_side:
                    if atom in left_equation[molecule_id]:
                        left_equation[molecule_id][atom] += multiplier
                    else:
                        left_equation[molecule_id][atom] = multiplier
                else:
                    if atom in right_equation[molecule_id]:
                        right_equation[molecule_id][atom] += multiplier
                    else:
                        right_equation[molecule_id][atom] = multiplier
                atom = ""
            elif char == "+":
                if left_side:
                    left_equation.append(dict())
                    if atom != "":
                        if atom in left_equation[molecule_id]:
                            left_equation[molecule_id][atom] += multiplier
                        else:
                            left_equation[molecule_id][atom] = multiplier
                else:
                    right_equation.append(dict())
                    if atom != "":
                        if atom in right_equation[molecule_id]:
                            right_equation[molecule_id][atom] += multiplier
                        else:
                            right_equation[molecule_id][atom] = multiplier
                molecule_id += 1
                atom = ""
                multiplier = 1
            elif char == "=":
                if left_side:
                    if atom != "":
                        if atom in left_equation[molecule_id]:
                            left_equation[molecule_id][atom] += multiplier
                        else:
                            left_equation[molecule_id][atom] = multiplier
                else:
                    if atom != "":
                        if atom in right_equation[molecule_id]:
                            right_equation[molecule_id][atom] += multiplier
                        else:
                            right_equation[molecule_id][atom] = multiplier
                left_side = False
                molecule_id = 0
                atom = ""
                multiplier = 1

            if not char.isdigit() and char != "+" and char != "=":
                atom += char

        if atom in right_equation[molecule_id]:
            right_equation[molecule_id][atom] += multiplier
        else:
            right_equation[molecule_id][atom] = multiplier

left_keys = list()
right_keys = list()

for molecules in left_equation:
    left_keys += list(molecules.keys())

for molecules2 in right_equation:
    right_keys += list(molecules2.keys())

if not set(left_keys).issubset(set(right_keys)):
    with open('team12_que_nerd/challenge39/result.txt', 'w+') as f:
        f.write("IMPOSSIBLE\n")
        sys.exit(1)

left_molecules = Counter()
right_molecules = Counter()

for molecules in left_equation:
    left_molecules += Counter(molecules)

for molecules2 in right_equation:
    right_molecules += Counter(molecules2)

left_molecules = OrderedDict(sorted(left_molecules.items(), key=itemgetter(1), reverse=True))
right_molecules = OrderedDict(sorted(right_molecules.items(), key=itemgetter(1), reverse=True))
molecules = OrderedDict()

if next(iter(left_molecules.values())) > next(iter(right_molecules.values())):
    molecules = left_molecules
else:
    molecules = right_molecules

for atom, frequency in molecules.items():
    left = left_molecules[atom]
    right = right_molecules[atom]
    print(right_equation)

    if left != right:
        if left > right:
            quo = decimal.Decimal(left / right).to_integral_value()
        else:
            quo = decimal.Decimal(right / left).to_integral_value()

        if left % right == 0:
            verify(quo, atom, '', right_equation)
        else:
            verify(quo, atom, left_equation, right_equation)

print(left_equation, right_equation)
