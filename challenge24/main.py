import sys
import math

INPUT = sys.argv[1]
OUTPUT = "team12_que_nerd/challenge24/result.txt"


def divisors(n) : 
   result = []
   for i in range(1, int(math.sqrt(n) + 1)): 
      if (n % i == 0) : 
         if (n / i == i) : 
            result.append(i)
         else: 
            result.append(i)
            result.append(int(n/i))
   return result


file = open(INPUT, "r")
data = file.readlines()
file.close()

file_output = open(OUTPUT, "w")


first = int(data[0].replace("\n", ""))


all_divs = divisors(first) 
if len(all_divs) > 2:
   file_output.write("-1")
else:
   all_divisors = divisors(first - 1)
   possible_primes = []
   for divisor in all_divisors:
      possible_primes.append(first + divisor)
      possible_primes.append(first - divisor)
   
   primes = []
   for possible in possible_primes:
      if len(divisors(possible)) == 2:
         primes.append(possible)

   file_output.write(str(sorted(primes))[1:-1].replace(",", ""))

file_output.write("\n")
file_output.close()
