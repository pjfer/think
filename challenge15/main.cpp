#include <bits/stdc++.h>
#include <math.h>
using namespace std;


int min_number = INT_MAX;
int steps(int source, int step, int dest,int counter) 
{ 
    if(counter >= min_number)
        return INT_MAX;
    
    if (source > dest)  
         return INT_MAX; 


    if (source == dest && step == 1 ) {
        min_number = counter;
        return counter;
    }
        
    
    
    int pos,neg,same;
    if(step == 0){
        pos = steps(source + (step + 1),  step + 1, dest, counter+1); 
        neg = INT_MAX;
        same = INT_MAX;
    }
    
    else {
        pos = steps(source + (step + 1),  step + 1, dest, counter+1);
        neg = steps(source + (step - 1),  step - 1, dest, counter+1);    
        same = steps(source + step ,  step, dest, counter+1);    
    }
    
    vector<int> a;
    a.push_back(pos);
    a.push_back(neg);
    a.push_back(same);
    
   

    return *min_element(a.begin(), a.end());  
} 

                                                             
int main(int argc, char** argv) {


    ifstream file;

    file.open(argv[1]);

    int x,y;
    file >> x >> y;


    int l = steps(x,0,y,0);


    ofstream outputFile("result.txt");

    outputFile << l << "" << endl;

    return 0;
}