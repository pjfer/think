#include <bits/stdc++.h>
#include <math.h>
using namespace std;

#define ull unsigned long long int 

ull decimalToBinary(ull N) 
{ 
  
    // To store the binary number 
    ull B_Number = 0; 
    ull cnt = 0; 
    while (N != 0) { 
        ull rem = N % 2; 
        ull c = pow(10, cnt); 
        B_Number += rem * c; 
        N /= 2; 
  
        // Count used to store exponent value 
        cnt++; 
    } 
  
    return B_Number; 
} 

int m(int n,int k,int number) {

    vector<ull> lista;
    
    ull smallest = (1<<k)-1;

    while (smallest < (1<<n) ) {

        int size = int(log2(smallest)) +1 ;
        
        if(size == n) {
            if (smallest % number == 0)
                lista.push_back(smallest);
        }
    


        ull lowbit = smallest&~(smallest-1);

        ull ones = smallest&~(smallest+lowbit);

        smallest = smallest+lowbit+(ones/lowbit/2);

    }
    return lista.size();
}
                                        
int main(int argc, char** argv) {


    ifstream file;

    file.open(argv[1]);

    int n , k ; 
    file >> n >> k;



    ofstream outputFile("result.txt");



    int size = m(n,n/2,k);

    outputFile << size << "" << endl;




    
    

    
    return 0;
}