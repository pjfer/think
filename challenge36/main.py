import sys
import collections

INPUT = sys.argv[1]
OUTPUT = "team12_que_nerd/challenge36/result.txt"


def verify_colors(data, from_node=-1, start_node=0, path=[]):
   connected_nodes = data[start_node][1].copy()
   if from_node in connected_nodes:
      connected_nodes.remove(from_node)
   
   new_path = path.copy()
   new_path.append(from_node)
   for connected_node in connected_nodes:
      if connected_node in path:
         return False
      if not verify_colors(data, start_node, connected_node, new_path):
         return False
   return True

def cum_sum(input_data):
   result = 0
   for l in input_data:
      result += l
   return result

def diff_from_input(data, number, current_node):
   result = data.copy()
   for i in data.keys():
      if number == 0:
         return result
      if data[i][0] > 0:
         result[i][0] = data[i][0] - 1
         result[current_node][1].append(i)
         result[i][1].append(current_node)

         number -= 1
   if number == 0:
      return result
   return False


file = open(INPUT, "r")
data = file.readlines()
file.close()

n = int(data[0].replace('\n', ''))
data_splited = data[1].split(' ')
data = {i: [int(data_splited[i]), []] for i in range(len(data_splited))}

file_result = open(OUTPUT, "w")
# current_possible_graph = data.copy()


while sum([value[0] for value in data.values()]) > 0:
   data = dict(collections.OrderedDict(sorted(data.items(), reverse=True, key=lambda x:x[1][0])))
   i = list(data.keys())[0]

   current_value_connect = data[i][0]
   if current_value_connect > 0:
      data[i][0] = 0
      result = diff_from_input(data, current_value_connect, i)
      if result:
        data = result
      else:
         file_result.write("no")
         break
else:
   file_result.write("yes" if verify_colors(data) else "no")

file_result.write("\n")
file_result.close()
