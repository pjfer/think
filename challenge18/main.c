#include<stdio.h>
#include<stdlib.h>
#include<math.h>

#define SAVE_DOC 1                                                                                                      

const unsigned long MAX_SAVE = 100;                                                                                     
unsigned long num_paths_saved = 0;                                                                                      
FILE *file;

const short MOVEMENT_1 = 2;
const short MOVEMENT_2 = 1;

unsigned int grid_size[2];
unsigned int **grid;

int horse_position[2];



/* Recursive function, where are calculated all the horse's possible moviments for a giver chess board (grid)
 *
 * Arguments:
 *    -> short move_x: horse's movement in grid's x axes
 *    -> short move_y: horse's movement in grid's y axes
 *    -> int step: current horse's step (useful to store in grid's array)
 *    -> short pass_throw_every_sqare: boolean value -> if true (>0), the horse is forced to visit every sqare of chess board; if false (<=0), the horse isn't forced to do that
 * Return:
 *    -> number of all different possible routes
 */
unsigned long long move(short move_x, short move_y, int step, short pass_throw_every_sqare){

   int new_position[] = {horse_position[0] + move_x,                                                                    
                         horse_position[1] + move_y};

   
   
      if(!pass_throw_every_sqare ||                                                                                     
         grid[horse_position[0]][horse_position[1]] == grid_size[0]*grid_size[1]){
         
         if(SAVE_DOC && (num_paths_saved < MAX_SAVE)){
            num_paths_saved++;
            for(int n1 = 0; n1 < grid_size[0]; n1++){
               for(int n2 = 0; n2 < grid_size[1]; n2++)
                  if (grid[n1][n2] < 11)
                     fprintf(file, "0%d\t", grid[n1][n2] - 1);
                  else
                     fprintf(file, "%d\t", grid[n1][n2] - 1);
               if (n1 < grid_size[0] - 1)
                  fprintf(file, "\n");
            }           
         }
         
         return 1;
      }

   

   if(new_position[0] < 0 || new_position[1] < 0 ||                                                                     
      new_position[0] > (grid_size[0] - 1) || new_position[1] > (grid_size[1] - 1) ||                                   
      grid[new_position[0]][new_position[1]] != 0)                                                                      
      return 0;


   unsigned long long num_success = 0;                                                                                  

   int last_horse_position[] = {horse_position[0], horse_position[1]};                                                  
   
   horse_position[0] = new_position[0];                                                                                 
   horse_position[1] = new_position[1];                                                                                 
   
   grid[horse_position[0]][horse_position[1]] = ++step;                                                                 
   int last_horse_position_marked[] = {horse_position[0], horse_position[1]};                                           

   /**************** all possible movements ****************/
   if (move(MOVEMENT_1, MOVEMENT_2, step, pass_throw_every_sqare))
      return 1;
   if (move(MOVEMENT_1, -MOVEMENT_2, step, pass_throw_every_sqare))
      return 1;
   if (move(-MOVEMENT_1, -MOVEMENT_2, step, pass_throw_every_sqare))
      return 1;
   if (move(-MOVEMENT_1, MOVEMENT_2, step, pass_throw_every_sqare))
      return 1;

   if (move(MOVEMENT_2, MOVEMENT_1, step, pass_throw_every_sqare))
      return 1;
   if (move(MOVEMENT_2, -MOVEMENT_1, step, pass_throw_every_sqare))
      return 1;
   if (move(-MOVEMENT_2, -MOVEMENT_1, step, pass_throw_every_sqare))
      return 1;
   if (move(-MOVEMENT_2, MOVEMENT_1, step, pass_throw_every_sqare))
      return 1;


   /**************** backtracking ****************/
   horse_position[0] = last_horse_position[0];
   horse_position[1] = last_horse_position[1];
   grid[last_horse_position_marked[0]][last_horse_position_marked[1]] = 0;


   return num_success;

}

int main(int argc, char** argv){
   /**************** initializations ****************/
   FILE *fp;
   int number[3];
   fp = fopen(argv[1], "r");
   for (int i = 0; i < 3; i++)
      fscanf(fp, "%d", &number[i]);

   short PASS_THROW_EVERY_SQUARE = 1;                                                                                   

   grid_size[0] = number[0];                                                                                                    
   grid_size[1] = number[1];                                                                                                    

   grid = (unsigned int **)malloc(grid_size[0]*sizeof(unsigned int *));                                                 
   for(int num = 0; num < grid_size[0]; num++)
      grid[num] = (unsigned int *)calloc(grid_size[1], sizeof(unsigned int));

   horse_position[0] = (int)(number[2]/grid_size[0] + 0.5);
   horse_position[1] = number[2]%grid_size[0];

   /**************** problem's resolution ****************/
   if(SAVE_DOC){                                                                                                        
      char *file_name = malloc(1000);
      sprintf(file_name, "result.txt");
      file = fopen(file_name, "w");
      free(file_name);
   }

   move(0, 0, 0, PASS_THROW_EVERY_SQUARE);

   if(SAVE_DOC){
      fprintf(file, "\n"); 
      fclose(file);
   }

}
