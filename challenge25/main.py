import sys
import string
from itertools import combinations


with open(sys.argv[1], 'r') as f:
    codebook = dict()
    word_counts = dict()
    punctuation = '''!()[]{};:'"\,<>./?@#$%^&*_~”“\n’'''
    words = f.read().split(" ")
    words = [word.translate(str.maketrans('', '', punctuation)) for word in words]
    codes = [str(value) for value in range(10)]
    codes += list(string.ascii_lowercase) + list(string.ascii_uppercase)
    tmp = list(combinations(codes, 2)) + list(combinations(codes, 3))
    codes += [''.join(tup) for tup in tmp]

    for word in words:
        if word in word_counts.keys():
            word_counts[word] += 1
        else:
            word_counts[word] = 1

    word_counts = { k: v for k, v in sorted(word_counts.items(), key=lambda item: item[1], reverse=True) }

    for word in word_counts:
        code = codes[0]

        if word not in codebook.keys() and code not in codebook.values():
            codebook[word] = code
            codes.remove(code)
        else:
            continue

    with open('team12_que_nerd/challenge25/result.txt', 'w+') as fp:
        fp.write(f"str(codebook)\n")