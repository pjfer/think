#include <bits/stdc++.h>
#include <math.h>
using namespace std;

#define ull unsigned long long int

vector<vector<bool>> visited(40,vector<bool>(40,false));


int counter2 = 0;
ull f(vector<vector<int>> grid,int x, int y, int x_end, int y_end,int last_value,string fds) {

	//cout << fds << endl;
	cout << x << " "  << y << endl; 
	
	if(x >= 40 ||  x < 0  || y >= 40  || y < 0 ){
		//visited[y][x] = true;
		return 0;
	}
		
	//cout << last_value << endl;
	//cout << grid[y][x] << endl;
	//cout <<  (grid[y][x] <= last_value) << endl;
	//cout << endl;
	
	//if (last_value > grid[y][x])
	//	last_value = grid[y][x];
	//cout << x_end << endl;

	if(visited[x][x] || grid[y][x] <= last_value) {
		return 0;

	}
	
	


	if( x == x_end  && y == y_end){
		return 1;
	}
		
	visited[y][x] = true;
	

	//ull sum = f(grid,x+1,y,x_end,y_end,grid[y][x]) + f(grid,x,y+1,x_end,y_end,grid[y][x]) + f(grid,x-1,y,x_end,y_end,grid[y][x])+ f(grid,x,y-1,x_end,y_end,grid[y][x]);
	ull sum = 	f(grid,x + 1 , y , x_end , y_end , grid[y][x],"x+1") +
				f(grid,x-1 , y , x_end , y_end , grid[y][x],"x-1") +
				f(grid,x , y + 1 , x_end , y_end , grid[y][x],"y+1") +
				f(grid,x , y - 1 , x_end , y_end , grid[y][x],"y-1");

	visited[y][x] = false;
	cout << sum << endl;
	return sum;
}

//ull f(vector<vector<int>> grid,int x , int y , int x_end, int y_end,vector<vector<bool>> visited){
//
//	if ( x < 0 || y < 0  || x >= 40 || y >= 40 ) return 0;
//
//	if (x == x_end && y == y_end) return 1;
//
//	if(visited[])
//
//}

bool isValidCell(int x, int y)
{
	if (x < 0 || y < 0 || x >= 40 || y >= 40)
		return false;

	return true;
}



void countPaths(vector<vector<int>> gride, int x, int y,int x_end,int y_end, vector<vector<int>> visited, int& count,int last_value)
{
	
	if (x == x_end && y == y_end) 
	{
		count++;
		return;
	}

	cout << x << " " << y << endl;
	visited[x][y] = 1;
	
	if (gride[x][y] <= last_value)
		return ;
	
	if (isValidCell(x, y) && gride[x][y]) 
	{
		 
		if (x + 1 < 40 && !visited[x + 1][y])
			countPaths(gride, x + 1, y,x_end,y_end, visited, count,last_value);
			
		
		if (x - 1 >= 0 && !visited[x - 1][y])
			countPaths(gride, x - 1, y,x_end,y_end, visited, count,last_value);
			
		
		if (y + 1 < 40 && !visited[x][y + 1])
			countPaths(gride, x, y + 1, x_end,y_end,visited, count,last_value);
			
		
		if (y - 1 >= 0 && !visited[x][y - 1])
			countPaths(gride, x, y - 1, x_end,y_end,visited, count,last_value);
	}
	
	
	visited[x][y] = 0;
}


int main(int argc, char** argv) {


    ifstream file;

    file.open(argv[1]);

	int start_x,start_y,end_x,end_y;

	file >> start_x >> start_y  >> end_x >> end_y;

	vector<vector<int>> grid(40,vector<int>(40,0));
	for(int y = 0 ; y < 40 ; y++) {
		for(int x = 0 ; x < 40 ; x++) {
			file >> grid[y][x];
		}
	}

	vector<vector<int>> visited(40,vector<int>(40,0));
	
	int counter2 = 0;

	countPaths(grid,start_x,start_y,end_x,end_y,visited,counter2,-1);

	//ull counter = f(grid,start_x,start_y,end_x,end_y,-1,"");
	//cout << counter << endl;
	cout << counter2 << endl;

	//cout << counter2 << endl;
    ofstream outputFile("result.txt");


    return 0;
}