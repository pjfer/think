import sys
import decimal


decimal.getcontext().rounding = decimal.ROUND_HALF_UP

with open(sys.argv[1], 'r') as f:
    for line in f:
        values = line.split(" ")
        k = int(values[0])
        m = int(values[1])

        if k == 1:
            print(decimal.Decimal(m * (m + 1) / 2).to_integral_value())

            with open('team12_que_nerd/challenge6/result.txt', 'w+') as fp:
                fp.write(str(decimal.Decimal(m * (m + 1) / 2).to_integral_value()))
        else:
            firework = decimal.Decimal(m / 2).to_integral_value() 
            total_firework = firework

            while(firework < m - 1):
                firework += decimal.Decimal((m - firework) / 2).to_integral_value() 
                total_firework += firework

            total_firework += m
            print(total_firework)

            with open('team12_que_nerd/challenge6/result.txt', 'w+') as fp:
                fp.write(str(total_firework))
                fp.write("\n")
