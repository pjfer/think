import sys
from collections import deque

stack = deque()

with open(sys.argv[1], 'r' ) as f:
    phrase = f.read()

output_string = list()
found = False
for i in phrase:
    if i == "\"" and not found:
        output_string.append("` `")
        found = True
    elif i == "\"" and found:
        output_string.append("''")
        found = False
    else:
        output_string.append(i)

PATH = "team12_que_nerd/challenge2/result.txt"

output_file = open(PATH,'w')
output_file.write(f"{''.join(output_string)}\n")
