#include <bits/stdc++.h>
#include <math.h>
using namespace std;



void f2(vector<string> lines, vector<vector<int>> visited, int x, int y, int x_end, int y_end,int x_max,int y_max, int& min_dist, int dist)
{
    
	if (x == x_end && y == y_end) 
	{
		min_dist = min(dist, min_dist);
		return;
	}

	
	visited[y][x] = 1;
    
	
	if ( x+1 < x_max && y < y_max && x+1>=0 && y>= 0 && lines[y].at(x+1) != '#'  &&  visited[y][x+1] == 0 )
		f2(lines, visited, x + 1, y, x_end, y_end,x_max,y_max, min_dist, dist+1);
 
		 
	if (x-1 < x_max && y < y_max && x-1>=0 && y>= 0 && lines[y].at(x-1) != '#'  &&  visited[y][x-1] == 0)
		f2(lines, visited, x - 1, y, x_end, y_end,x_max,y_max, min_dist, dist+1);
	 
	
	if (x < x_max && y + 1 < y_max && x>=0 && y + 1>= 0 && lines[y+1].at(x) != '#'  &&  visited[y+1][x] == 0)
		f2(lines, visited, x, y + 1, x_end, y_end,x_max,y_max, min_dist, dist+1);
	 
	
	if (x < x_max && y - 1 < y_max && x>=0 && y - 1>= 0 && lines[y-1].at(x) != '#'  &&  visited[y-1][x] == 0)
		f2(lines, visited, x, y - 1, x_end, y_end,x_max,y_max, min_dist, dist+1);
	 
	
	visited[y][x] = 0;
}

int f(vector<string> lines,int x , int y, int x_end, int y_end ,int x_max, int y_max,int counter) {
    
    
    
    if (x == x_end && y == y_end) {
        // Goal:   
        counter+=1;
        
        return true;
    }
    
    if ( x < x_max &&  y < y_max && lines[y].at(x) != '#'){
        //Rip
        //return false;
    
       
        counter+=1;

        if(f(lines,x+1,y,x_end, y_end,x_max, y_max,counter) == true) 
            return true;
        

        if(f(lines,x,y+1,x_end,  y_end, x_max,  y_max,counter) == true) 
            return true;
        

        if(f(lines,x-1,y,x_end,  y_end, x_max,  y_max,counter) == true) 
            return true;
        

        if(f(lines,x,y-1,x_end,  y_end, x_max,  y_max,counter) == true) 
            return true;
    

    counter-=1;
    return false;

    }

    return false;

}






int main(int argc, char** argv) {


    ifstream file;

    file.open(argv[1]);

    int linhas , colunas ; 

    vector<string> lines;

    int y = 0;
    

    int s_x = -1;
    int s_y = -1;

    int x_x = -1;
    int x_y = -1;

    for (string line; getline(file, line); ) {
            size_t  index_s = line.find('S');
            size_t index_x = line.find('X');
            if (index_s != string::npos){
                s_y = y;
                s_x = int(index_s);
            }
            if (index_x != string::npos){
                x_y = y;
                x_x = int(index_x);
            }

            lines.push_back(line);
            y++;
    }

    int x = lines[0].size();
    
    int counter = 0;
    

    int min_dist = INT_MAX;
    vector<vector<int>> visited(y,vector<int>(x,0));

    f2(lines,visited,s_x,s_y,x_x,x_y,x,y,min_dist,0);
    

    ofstream outputFile("result.txt");

    outputFile << "Caminho mais curto: " << min_dist << endl;

    return 0;
}