import sys
from math import sqrt


with open(sys.argv[1], 'r' ) as f:
    n,k = list(map(int, next(f).split()))


def f(n):
    return ((1+sqrt(5))**n-(1-sqrt(5))**n)/(2**n*sqrt(5))


def n_fib(n):
    lista = []
    for i in range(1,n+1):
        lista.append(int(f(i)))
    
    return lista


current_index = k
lista = n_fib(k+1)


while True:
    value = 0
    if k == 1:
        value+=lista[current_index]*2
    else:
        for i in range(k):
            value+=lista[current_index-i]

    lista.append(value)
    current_index+=1
    if(value >= n):
        break

OUTPUT = "result.txt"

output_file = open(OUTPUT,'w')

if n in lista:
    output_file.write("lose")

else:
    for i in range(0,len(lista)-1):
        if lista[i+1] - lista[i] > 1:
            output_file.write(str(lista[i]+1))
            break

output_file.write("\n")
output_file.close()
