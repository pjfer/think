import sys
import re


def calc_x(a,b,c,d,e,f):
    return int((c*e - b*f)) / int((a*e - b*d))

def calc_y(a,b,c,d,e,f):
    return (a*f - c*d) / (a*e - b * d) 


lista = []

with open(sys.argv[1], 'r' ) as f:
    lista = [x.rstrip().split(' ') for x in f.readlines()]

all_mapas = []
all_somas = []
for line in lista:
    multiple = 1
    minus = 1
    mapa = {}
    soma = 0
    for i in range(len(line)):
        
        val = line[i]
        
        if val.isdigit():
            soma+=int(val) * (-multiple)
        elif len(val) > 1 and ('x' in val or 'y' in val):
            
            if val[-1] not in mapa:
                mapa[val[-1]] = 0 
            
            mapa[val[-1]]+=int(val[:-1])  * multiple * minus
            if(minus == -1):
                minus = 1
        elif 'x'  == val or 'y' == val:
            if val[-1] not in mapa:
                mapa[val[-1]] = 0 
            mapa[val[-1]]+=int(1)  * multiple
        elif val == '=':
            multiple = -1
        elif val == '-':
            minus = - 1
        



    all_mapas.append(mapa)
    all_somas.append(soma)

print(all_mapas)
print(all_somas)
a ,b = list(all_mapas[0].values())
d,e = list(all_mapas[1].values())
c = all_somas[0]
f = all_somas[1]

PATH = "result.txt"

output_file = open(PATH,'w')



try:
    output_file.write(str(int(calc_x(a,b,c,d,e,f))) + '\n')
    output_file.write(str(int(calc_y(a,b,c,d,e,f)))+'\n')
except Exception as e:
    print(e)
    output_file.write("don't know\n")
    output_file.write("don't know\n")

output_file.write("\n")           

                