#include <bits/stdc++.h>
#include <math.h>
using namespace std;


                                                                        
int main(int argc, char** argv) {


    ifstream file;

    file.open(argv[1]);

    int l;
    file >> l;


    map<int,int> trains;

    for(int i = 0 ; i < l ; i++){
        int value ; 
        file >> value;
        trains.insert({value,i+1});
    }

    int value = 0;

    for(int i = 1 ; i < l ; i++) {


        int index = trains[i];

        if (index == i)
            continue;

        else {
            int h = abs(index - i);
            value+= h;
            for(int j = i  ; j <  index ; j++ ) {
                for (map<int, int>::iterator it = trains.begin(); it != trains.end(); ++it)
                {   
                    if (it->second <=j ) {
                        trains[it->first]++;
                    }
                }
                
            }
            trains[i] = i;
        }
        
    }

    
    ofstream outputFile("result.txt");
    
    outputFile << "Optimal train swapping takes " << value << " swaps."  << endl;
    
    return 0;
}