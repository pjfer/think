import sys


def moveDisks(diskPositions, largestToMove, targetPeg,f):
    for badDisk in range(largestToMove, len(diskPositions)):

        currentPeg = diskPositions[badDisk]         
        if currentPeg != targetPeg:
            otherPeg = 3 - targetPeg - currentPeg
            moveDisks(diskPositions, badDisk+1, otherPeg,f)
            f.write(str(len(diskPositions) - badDisk) + " -> " + str(targetPeg) + "\n")
            #print (len(diskPositions) - badDisk, "->", targetPeg)
            diskPositions[badDisk]=targetPeg
            moveDisks(diskPositions, badDisk+1, targetPeg,f)

            break


f = open(sys.argv[1],'r')
lines = f.readlines()
lista = []
mapa = {}


for i in range(len(lines)):
    line = lines[i]
    line_splitted = line.split()
    if line_splitted != []:
        for x in line_splitted:
            if x.isdigit():
                mapa[int(x)] = i
    


indexs = [-1 for x in range(len(mapa))]
for v in mapa:
    indexs[v-1] = mapa[v]

towers = len(indexs)
max_value = len(indexs)

PATH = "team12_que_nerd/challenge19/result.txt"
f = open(PATH,'w')
moveDisks(indexs[::-1],0,towers-1,f)

f.write("\n")
f.close()


